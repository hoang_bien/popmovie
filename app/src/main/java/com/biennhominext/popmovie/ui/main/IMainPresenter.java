package com.biennhominext.popmovie.ui.main;

import com.biennhominext.popmovie.ui.base.IBasePresenter;

/**
 * Created by bien on 7/4/2017.
 */

public interface IMainPresenter<V extends IMainView> extends IBasePresenter<V> {
    void onViewPrepared();
}
