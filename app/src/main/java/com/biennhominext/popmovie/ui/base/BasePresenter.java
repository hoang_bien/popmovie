package com.biennhominext.popmovie.ui.base;

import com.biennhominext.popmovie.data.DbManager;
import com.biennhominext.popmovie.utils.SchedulerProvider;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;

/**
 * Created by bien on 7/4/2017.
 */

public class BasePresenter<V extends IBaseView> implements IBasePresenter<V>{
    private final DbManager mDbManager;
    private final SchedulerProvider mScheduler;
    private final CompositeDisposable mDisposable;
    private V mView;

    @Inject
    public BasePresenter(DbManager dbManager, SchedulerProvider scheduler, CompositeDisposable compositeDisposable){
        mDbManager = dbManager;
        mScheduler = scheduler;
        mDisposable = compositeDisposable;
    }

    @Override
    public void onAttach(V view) {
        mView = view;
    }

    public void onDetach(){
        mDisposable.clear();
        mView = null;
    }

    public DbManager getDbManager() {
        return mDbManager;
    }

    public CompositeDisposable getDisposable() {
        return mDisposable;
    }

    public SchedulerProvider getScheduler() {
        return mScheduler;
    }

    public V getView() {
        return mView;
    }
}
