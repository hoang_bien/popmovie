package com.biennhominext.popmovie.ui.main;

import com.biennhominext.popmovie.data.db.model.Movie;
import com.biennhominext.popmovie.ui.base.IBaseView;

import java.util.List;

/**
 * Created by bien on 7/4/2017.
 */

public interface IMainView extends IBaseView {
    void updateMovies(List<Movie> movies);
    void showLoading();
    void hideLoading();
}
