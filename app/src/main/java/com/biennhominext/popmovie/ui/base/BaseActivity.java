package com.biennhominext.popmovie.ui.base;

import android.annotation.TargetApi;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.biennhominext.popmovie.MyApp;
import com.biennhominext.popmovie.R;
import com.biennhominext.popmovie.di.component.ActivityComponent;
import com.biennhominext.popmovie.di.component.DaggerActivityComponent;
import com.biennhominext.popmovie.di.module.ActivityModule;
import com.biennhominext.popmovie.utils.NetworkUtils;

import butterknife.Unbinder;

/**
 * Created by bien on 7/4/2017.
 */

public abstract class BaseActivity extends AppCompatActivity implements IBaseView {
    private ActivityComponent mActivityComponent;
    private Unbinder mUnbider;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mActivityComponent = DaggerActivityComponent.builder()
                .activityModule(new ActivityModule(this))
                .applicationComponent(((MyApp)getApplication()).getApplicationComponent())
                .build();

    }

    public ActivityComponent getActivityComponent() {
        return mActivityComponent;
    }

    @TargetApi(Build.VERSION_CODES.M)
    public void requestPermissionSafety(String[] permissions, int requestCode){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
            requestPermissions(permissions, requestCode);
        }
    }

    protected void setUnbinder(Unbinder unbider){
        mUnbider = unbider;
    }

    @TargetApi(Build.VERSION_CODES.M)
    public boolean hasPermission(String permission){
        return Build.VERSION.SDK_INT < Build.VERSION_CODES.M
                || checkSelfPermission(permission) == PackageManager.PERMISSION_GRANTED;
    }

    public void showSnackbar(String msg){
        Snackbar.make(findViewById(android.R.id.content),msg,Snackbar.LENGTH_SHORT).show();
    }


    @Override
    public void onError(@StringRes int resId) {
        onError(getString(resId));
    }

    @Override
    public void onError(String message) {
        if (message != null){
            showSnackbar(message);
        }else {
            showSnackbar(getString(R.string.unknown_error));
        }
    }

    public void showMessage(String msg){
        if (msg != null){
            Toast.makeText(this,msg,Toast.LENGTH_SHORT).show();
        }else {
            Toast.makeText(this, R.string.unknown_error,Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void showMessage(@StringRes int resId) {
        showMessage(getString(resId));
    }

    @Override
    public boolean isNetworkConnected() {
        return NetworkUtils.isNetworkConnected(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mUnbider != null) {
            mUnbider.unbind();
        }
    }

    public abstract void init();



}
