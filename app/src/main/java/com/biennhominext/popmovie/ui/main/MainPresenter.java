package com.biennhominext.popmovie.ui.main;

import android.util.Log;

import com.biennhominext.popmovie.data.DbManager;
import com.biennhominext.popmovie.ui.base.BasePresenter;
import com.biennhominext.popmovie.utils.SchedulerProvider;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;

/**
 * Created by bien on 7/4/2017.
 */

public class MainPresenter<V extends IMainView> extends BasePresenter<V> implements IMainPresenter<V> {
    @Inject
    public MainPresenter(DbManager dbManager, SchedulerProvider scheduler, CompositeDisposable compositeDisposable) {
        super(dbManager, scheduler, compositeDisposable);
    }

    @Override
    public void onViewPrepared() {
        getView().showLoading();
        getDisposable().add(getDbManager()
                .getAllMovies()
                .subscribeOn(getScheduler().io())
                .observeOn(getScheduler().ui())
                .doOnNext(__->getView().hideLoading())
                .subscribe(getView()::updateMovies,
                        err-> Log.e("MainActivity",err.getMessage(),err)));
    }
}
