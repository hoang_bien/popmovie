package com.biennhominext.popmovie.ui.base;

/**
 * Created by bien on 7/4/2017.
 */

public interface IBasePresenter<T extends IBaseView> {
    void onAttach(T view);
    void onDetach();
}
