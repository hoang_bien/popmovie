package com.biennhominext.popmovie.ui.base;

import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * Created by bien on 7/4/2017.
 */

public abstract class BaseViewHolder extends RecyclerView.ViewHolder {
    private int mCurrentPosition;

    public BaseViewHolder(View itemView) {
        super(itemView);
    }

    public void onBind(int pos){
        mCurrentPosition = pos;
        clear();
    }

    protected abstract void clear();

    public int getCurrentPosition() {
        return mCurrentPosition;
    }
}
