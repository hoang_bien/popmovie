package com.biennhominext.popmovie.ui.main;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.biennhominext.popmovie.R;
import com.biennhominext.popmovie.data.db.model.Movie;
import com.biennhominext.popmovie.data.network.ApiEndpoint;
import com.biennhominext.popmovie.ui.base.BaseViewHolder;
import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by bien on 7/4/2017.
 */

public class MovieAdapter extends RecyclerView.Adapter<BaseViewHolder> {

    public static final int VIEW_TYPE_NORMAL = 1;
    public static final int VIEW_TYPE_EMPTY = 0;
    private List<Movie> movies;

    public MovieAdapter(){
        movies = new ArrayList<>();
    }

    @Override
    public BaseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case VIEW_TYPE_NORMAL:
                return new MovieViewHolder(LayoutInflater.from(
                        parent.getContext()).inflate(R.layout.item_movie_view, parent, false));

            case VIEW_TYPE_EMPTY:
            default:
                return new EmptyViewHolder(LayoutInflater.from(
                        parent.getContext()).inflate(R.layout.item_empty_view, parent, false));
        }
    }

    @Override
    public void onBindViewHolder(BaseViewHolder holder, int position) {
        holder.onBind(position);
    }

    @Override
    public int getItemViewType(int position) {
        return isEmpty() ? VIEW_TYPE_EMPTY : VIEW_TYPE_NORMAL;
    }

    @Override
    public int getItemCount() {
        return isEmpty() ? 1 : movies.size();
    }

    private boolean isEmpty() {
        return movies == null || movies.size() == 0;
    }

    public void addItems(List<Movie> newItems){
        movies.addAll(newItems);
        notifyDataSetChanged();
    }

    public class EmptyViewHolder extends BaseViewHolder {

        @BindView(R.id.btn_retry)
        Button retryButton;

        @BindView(R.id.tv_message)
        TextView messageTextView;

        public EmptyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @Override
        protected void clear() {

        }

        @OnClick(R.id.btn_retry)
        void onRetryClick() {

        }
    }

    public class MovieViewHolder extends BaseViewHolder {
        @BindView(R.id.image_poster)
        ImageView posterImageView;
        @BindView(R.id.tv_title)
        TextView titleTextView;
        @BindView(R.id.tv_overview)
        TextView overviewTextView;

        public MovieViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }

        @Override
        public void onBind(int pos) {
            super.onBind(pos);
            final Movie movie = movies.get(pos);
            Glide.with(itemView.getContext())
                    .load(ApiEndpoint.BASE_IMG_PATH + movie.getPosterPath())
                    .crossFade()
                    .centerCrop()
                    .into(posterImageView);
            titleTextView.setText(movie.getTitle());
            overviewTextView.setText(movie.getOverview());

        }

        @Override
        protected void clear() {

        }
    }
}
