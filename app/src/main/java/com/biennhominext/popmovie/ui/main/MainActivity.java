package com.biennhominext.popmovie.ui.main;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.biennhominext.popmovie.R;
import com.biennhominext.popmovie.data.db.model.Movie;
import com.biennhominext.popmovie.ui.base.BaseActivity;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends BaseActivity implements IMainView {
    @Inject
    LinearLayoutManager mLayoutMgr;
    @Inject
    MovieAdapter mAdapter;
    @Inject
    IMainPresenter<IMainView> mPresenter;

    @BindView(R.id.refresh_movies)
    SwipeRefreshLayout mRefresh;

    @BindView(R.id.recycler)
    RecyclerView mRecycler;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(view -> Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                .setAction("Action", null).show());
        setUnbinder(ButterKnife.bind(this));
        getActivityComponent().inject(this);
        mPresenter.onAttach(this);
        init();
    }

    @Override
    public void init() {
        mRecycler.setLayoutManager(mLayoutMgr);
        mRecycler.setAdapter(mAdapter);
        showLoading();
        mPresenter.onViewPrepared();
    }

    @Override
    public void updateMovies(List<Movie> movies) {
        mAdapter.addItems(movies);
    }

    @Override
    public void showLoading() {
        mRecycler.setVisibility(View.INVISIBLE);
        mRefresh.setRefreshing(true);
    }

    @Override
    public void hideLoading() {
        mRefresh.setRefreshing(false);
        mRecycler.setVisibility(View.VISIBLE);
    }
}
