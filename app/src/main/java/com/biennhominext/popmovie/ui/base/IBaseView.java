package com.biennhominext.popmovie.ui.base;

import android.support.annotation.StringRes;

/**
 * Created by bien on 7/4/2017.
 */

public interface IBaseView {
    void onError(@StringRes int resId);

    void onError(String message);

    void showMessage(String message);

    void showMessage(@StringRes int resId);

    boolean isNetworkConnected();

}
