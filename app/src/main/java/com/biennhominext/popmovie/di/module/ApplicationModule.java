package com.biennhominext.popmovie.di.module;

import android.app.Application;
import android.arch.persistence.room.Room;
import android.content.Context;

import com.biennhominext.popmovie.data.AppDbManager;
import com.biennhominext.popmovie.data.DbManager;
import com.biennhominext.popmovie.data.db.DbHelper;
import com.biennhominext.popmovie.data.db.MovieDbHelper;
import com.biennhominext.popmovie.data.network.ApiEndpoint;
import com.biennhominext.popmovie.data.network.ApiHelper;
import com.biennhominext.popmovie.di.ApplicationContext;
import com.biennhominext.popmovie.di.DatabaseInfo;
import com.biennhominext.popmovie.utils.AppConstants;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by bien on 6/30/2017.
 */
@Module
public class ApplicationModule {
    private Application mApplication;

    public ApplicationModule(Application application) {
        mApplication = application;
    }

    @Provides

    public Application provideApplication() {
        return mApplication;
    }

    @Provides
    @ApplicationContext
    public Context provideContext() {
        return mApplication;
    }

    @Provides
    @DatabaseInfo
    public String provideDatabaseName() {
        return AppConstants.DB_NAME;
    }

    @Provides
    @Singleton
    public ApiHelper provideApiHelper(Retrofit retrofit) {
        return retrofit.create(ApiHelper.class);
    }

    @Provides
    @Singleton
    public DbHelper provideDbHelper(@ApplicationContext Context context,@DatabaseInfo String name) {
        return Room.databaseBuilder(context,DbHelper.class,name).build();
    }

    @Provides
    @Singleton
    public Gson provideGson(){
        return new GsonBuilder().create();
    }

    @Provides
    @Singleton
    public DbManager provideDbManager(AppDbManager appDbManager){
        return appDbManager;
    }

    @Provides
    @Singleton
    public Retrofit provideRetrofit(Gson gson){
        return new Retrofit.Builder()
                .baseUrl(ApiEndpoint.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();
    }

}
