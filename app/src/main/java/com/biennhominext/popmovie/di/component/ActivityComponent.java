package com.biennhominext.popmovie.di.component;

import android.app.Application;

import com.biennhominext.popmovie.di.PerActivity;
import com.biennhominext.popmovie.di.module.ActivityModule;
import com.biennhominext.popmovie.ui.main.MainActivity;

import dagger.Component;

/**
 * Created by bien on 7/4/2017.
 */
@PerActivity
@Component(dependencies = ApplicationComponent.class, modules = ActivityModule.class)
public interface ActivityComponent {
    void inject(MainActivity activity);
}
