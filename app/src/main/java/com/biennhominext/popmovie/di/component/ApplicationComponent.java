package com.biennhominext.popmovie.di.component;

import android.app.Application;
import android.content.Context;

import com.biennhominext.popmovie.data.DbManager;
import com.biennhominext.popmovie.di.ApplicationContext;
import com.biennhominext.popmovie.di.module.ApplicationModule;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by bien on 6/30/2017.
 */
@Singleton
@Component(modules = {ApplicationModule.class})
public interface ApplicationComponent {

    void inject(Application myApp);

    @ApplicationContext Context context();

    Application application();

    DbManager appDbManger();
}
