package com.biennhominext.popmovie.di.module;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;

import com.biennhominext.popmovie.di.ApplicationContext;
import com.biennhominext.popmovie.di.PerActivity;
import com.biennhominext.popmovie.ui.main.IMainPresenter;
import com.biennhominext.popmovie.ui.main.IMainView;
import com.biennhominext.popmovie.ui.main.MainPresenter;
import com.biennhominext.popmovie.ui.main.MovieAdapter;
import com.biennhominext.popmovie.utils.AppSchedulerProvider;
import com.biennhominext.popmovie.utils.SchedulerProvider;

import dagger.Module;
import dagger.Provides;
import io.reactivex.disposables.CompositeDisposable;

/**
 * Created by bien on 7/4/2017.
 */
@Module
public class ActivityModule {
    private AppCompatActivity mActivity;

    public ActivityModule(AppCompatActivity activity) {
        this.mActivity = activity;
    }

    @Provides
    @ApplicationContext
    Context provideContext() {
        return mActivity;
    }

    @Provides
    AppCompatActivity provideActivity() {
        return mActivity;
    }

    @Provides
    SchedulerProvider provideSchedulerProvider() {
        return new AppSchedulerProvider();
    }

    @Provides
    CompositeDisposable provideCompositeDisposable() {
        return new CompositeDisposable();
    }

    @PerActivity
    @Provides
    IMainPresenter<IMainView> provideMainPresenter(MainPresenter<IMainView> mainPresenter){
        return mainPresenter;
    }
    @Provides
    MovieAdapter provideMovieAdapter() {
        return new MovieAdapter();
    }

    @Provides
    LinearLayoutManager provideLinearLayoutManger(AppCompatActivity appCompatActivity) {
        return new LinearLayoutManager(appCompatActivity);
    }
}
