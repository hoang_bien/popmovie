package com.biennhominext.popmovie.data;

import com.biennhominext.popmovie.data.db.DbHelper;
import com.biennhominext.popmovie.data.db.MovieDbHelper;
import com.biennhominext.popmovie.data.db.model.Movie;
import com.biennhominext.popmovie.data.network.ApiHelper;
import com.biennhominext.popmovie.utils.AppConstants;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Flowable;

/**
 * Created by bien on 6/30/2017.
 */
@Singleton
public class AppDbManager implements DbManager{
    private final MovieDbHelper mMovieDbHelper;
    private final ApiHelper mApiHelper;

    @Inject
    public AppDbManager(DbHelper dbHelper, ApiHelper apiHelper){
        mMovieDbHelper = dbHelper.movieDao();
        this.mApiHelper = apiHelper;
    }

    @Override
    public Flowable<List<Movie>> getAllMovies() {
        Flowable<List<Movie>> movieApi = getAndSaveMovies().onErrorReturnItem(new ArrayList<>());
        Flowable<List<Movie>> movieDb = mMovieDbHelper.getAllMovies();

        return Flowable.concat(movieApi,movieDb)
                .filter(movies -> movies.size() != 0)
                .take(1);
    }

    private Flowable<List<Movie>> getAndSaveMovies() {
        return mApiHelper.getAllMovies(AppConstants.POP_DESC,1)
                .flatMap(movieResponse-> Flowable.fromIterable(movieResponse.getResults())
                        .doOnNext(movie -> mMovieDbHelper.insertMovie(movie))
                        .buffer(128));
    }
}
