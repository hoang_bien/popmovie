package com.biennhominext.popmovie.data.network.model;

import com.biennhominext.popmovie.data.db.model.Movie;

import java.util.List;

/**
 * Created by bien on 6/30/2017.
 */

public class MovieResponse {
    private int pages;
    private List<Movie> results;

    public int getPages() {
        return pages;
    }

    public void setPages(int pages) {
        this.pages = pages;
    }

    public List<Movie> getResults() {
        return results;
    }

    public void setResults(List<Movie> results) {
        this.results = results;
    }
}
