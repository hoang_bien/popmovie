package com.biennhominext.popmovie.data;

import com.biennhominext.popmovie.data.db.model.Movie;

import java.util.List;

import io.reactivex.Flowable;

/**
 * Created by bien on 6/30/2017.
 */

public interface DbManager {
    Flowable<List<Movie>> getAllMovies();
}
