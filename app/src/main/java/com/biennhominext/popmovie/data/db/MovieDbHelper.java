package com.biennhominext.popmovie.data.db;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import com.biennhominext.popmovie.data.db.model.Movie;

import java.util.List;

import io.reactivex.Flowable;

/**
 * Created by bien on 6/30/2017.
 */

@Dao
public interface MovieDbHelper {
    @Query("SELECT * FROM movie")
    Flowable<List<Movie>> getAllMovies();
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertMovies(Movie... movie);
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertMovie(Movie movie);


}
