package com.biennhominext.popmovie.data.network;

/**
 * Created by bien on 6/30/2017.
 */

public final class ApiEndpoint {
    public static final String BASE_URL = "https://api.themoviedb.org/3/";
    public static final String BASE_IMG_PATH = "http://image.tmdb.org/t/p/w185/";

}
