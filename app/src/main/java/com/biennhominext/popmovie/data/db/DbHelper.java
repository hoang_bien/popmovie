package com.biennhominext.popmovie.data.db;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

import com.biennhominext.popmovie.data.db.model.Movie;

/**
 * Created by bien on 6/30/2017.
 */
@Database(entities = {Movie.class},version = 1)
public abstract class DbHelper extends RoomDatabase{
    public abstract MovieDbHelper movieDao();
}
