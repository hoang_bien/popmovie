package com.biennhominext.popmovie.data.network;


import com.biennhominext.popmovie.data.network.model.MovieResponse;
import com.biennhominext.popmovie.utils.AppConstants;

import java.util.List;

import io.reactivex.Flowable;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by bien on 6/30/2017.
 */

public interface ApiHelper {
    @GET("discover/movie?api_key=" + AppConstants.API_KEY)
    Flowable<MovieResponse> getAllMovies(@Query("sort_by") String sortOrder, @Query("page") int page);
}
