package com.biennhominext.popmovie.utils;

/**
 * Created by bien on 6/30/2017.
 */

public class AppConstants {
    public static final String DB_NAME = "movies";
    public static final String API_KEY = "de4060d655f32fa2010d0a6f116aa490";

    public static final String POP_DESC = "popularity.desc";
    public static final String POP_ASC = "popularity.asc";
}
