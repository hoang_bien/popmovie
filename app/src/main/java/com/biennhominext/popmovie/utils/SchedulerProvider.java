package com.biennhominext.popmovie.utils;

import io.reactivex.Scheduler;

/**
 * Created by bien on 7/4/2017.
 */

public interface SchedulerProvider {
    Scheduler ui();
    Scheduler io();
    Scheduler computation();
}
