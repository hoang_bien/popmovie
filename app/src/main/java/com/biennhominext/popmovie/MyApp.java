package com.biennhominext.popmovie;

import android.app.Application;
import android.util.Log;

import com.biennhominext.popmovie.data.DbManager;
import com.biennhominext.popmovie.di.component.ApplicationComponent;
import com.biennhominext.popmovie.di.component.DaggerApplicationComponent;
import com.biennhominext.popmovie.di.module.ApplicationModule;

import javax.inject.Inject;

import retrofit2.http.Path;

/**
 * Created by bien on 6/30/2017.
 */

public class MyApp extends Application {


    private ApplicationComponent mApplicationComponent;
    @Override
    public void onCreate() {
        super.onCreate();

        mApplicationComponent = DaggerApplicationComponent.builder()
                .applicationModule(new ApplicationModule(this))
                .build();


        mApplicationComponent.inject(this);

    }

    public ApplicationComponent getApplicationComponent() {
        return mApplicationComponent;
    }
}

